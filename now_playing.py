import obspython as obs
import subprocess
from os.path import exists

def script_description():
    return  """Now playing grabber for Linux!\nGets what song is playing right now and puts information into text."""

# On start this happens
def start():
    playerctl = exists("/usr/bin/playerctl")    # Check if playerctl even exists
    if playerctl == True:
        file = exists("/tmp/now_playing.txt")   # Check if the file in /tmp exists
        if file == False:   # If file in /tmp does not exist, create it
            f = open("/tmp/now_playing.txt", "w")
            f.write("")
            f.close()
        playing()
    else:   # If playerctl is not found, let it know into file
        f = open("/tmp/now_playing.txt", "w")
        f.write("playerctl is probably not installed")
        f.close()

def playing():
    stat = subprocess.run(["playerctl","status"], capture_output=True, text=True)   # Determine if anything is playing
    status = stat.stdout.strip("\n")    # Strip status of line break
    print (status)  # Just print something to show script is alive :)
    f = open("/tmp/now_playing.txt", "r")   # Read from file in /tmp folder
    if status == "Playing": # If playerctl returns playing status, do following
        tit = subprocess.run(["playerctl", "metadata", "title"], capture_output=True, text=True)    # Get the song title
        art = subprocess.run(["playerctl", "metadata", "artist"], capture_output=True, text=True)   # Get Artist name
        title = tit.stdout.strip("\n")  # Strip title of line break
        artist = art.stdout.strip("\n") # Strip artist name of line break
        # Write everything into text file
        f = open("/tmp/now_playing.txt", "w")   # Open file in write mode
        f.write("♪ " + artist + ": " + title)  # Write artist and song in file
        f.close()   # Close file
        # Write everything directly into text source (not working rn)
        #settings = obs.obs_data_create()
        #obs.obs_data_set_string(settings, "now_playing", "now_playing")
        #obs.obs_data_release(settings)
    else:   # If nothing is playing, do following
        if f.read() != "": # If file in tmp is not empty
            f = open("/tmp/now_playing.txt", "w")   # Open file for write
            f.write("") # Delete everything inside
            f.close()   # Close file
    loop()

def loop():
    obs.timer_remove(playing)
    obs.timer_add(playing, 5 * 1000)
    
start()
