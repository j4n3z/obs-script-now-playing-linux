# Now playing script

Python script to display what song is playing right now.

## How it works

The script uses `playerctl` package and relies on media player to correctly report played media.
After getting artist and song name from playerctl the script dumps small text file `/tmp/now_playing.txt` which can be read by text source in OBS and then sleeps for 5 seconds.
If no song is played, script clears the txt file so text visibly disappears.

## ToDo

- [ ] Rewrite the script so it dumps now playing directly to text source.

# Random MOTD script

Python script to read list of texts and display random line

## How it works

The script reads given `file.txt`, picks random line from the file and dumps it in chosen text source.
In settings there is also option to choose refresh interval between 1s and 1h.