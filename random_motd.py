import obspython as obs
import subprocess
import linecache
from os.path import exists
from os.path import expanduser
from random import randrange

home = expanduser('~')

def script_defaults(settings):
    obs.obs_data_set_default_string(settings, "motd_path", "")
    obs.obs_data_set_default_string(settings, "source_name", "")
    obs.obs_data_set_default_int(settings, "timing", 10)

def script_properties():
    props = obs.obs_properties_create()
    #source = obs.obs_get_source_by_name(src)
    sources = obs.obs_enum_sources()
    #source_id = obs.obs_source_get_unversioned_id(source)
    obs.obs_properties_add_path(props, "motd_path", "Path to text file", obs.OBS_PATH_FILE, "", home)
    obs.obs_properties_add_int(props, "timing", "Change frequency (s)", 1, 3600, 1)
    p = obs.obs_properties_add_list(props, "source_name", "Text Source", obs.OBS_COMBO_TYPE_EDITABLE, obs.OBS_COMBO_FORMAT_STRING)
    for source in sources:
        source_id = obs.obs_source_get_unversioned_id(source)
        print(source_id)
        if source_id == "text_gdiplus" or source_id == "text_ft2_source":
            name = obs.obs_source_get_name(source)
            obs.obs_property_list_add_string(p, name, name)
    obs.source_list_release(sources)
    return props

def script_description():
    return """Displays random motd which was read from text file"""

def script_update(settings):
    global motd_path
    global src
    global refresh
    motd_path = obs.obs_data_get_string(settings, "motd_path")
    src = obs.obs_data_get_string(settings, "source_name")
    refresh = obs.obs_data_get_int(settings, "timing")
    obs.timer_remove(pickamotto)
    start()

def script_load(settings):
    global motd_path
    global src
    global refresh
    motd_path = obs.obs_data_get_string(settings, "motd_path")
    src = obs.obs_data_get_string(settings, "source_name")
    refresh = obs.obs_data_get_int(settings, "timing")
    start()

def start():
    motd = exists(motd_path)    # Check if motd list exists
    if motd == True:
        pickamotto()
    else:                       # If motd list is not found, tell me
        fnf()

def fnf():
    # Manipulate text source
    source = obs.obs_get_source_by_name(src)
    settings = obs.obs_data_create()
    obs.obs_data_set_string(settings, "text", "File not found :(")
    obs.obs_source_update(source, settings)
    obs.obs_data_release(settings)
    obs.obs_source_release(source)
    loop()

def pickamotto():
    file = exists(motd_path)
    if file == False:
        fnf()
    else:
        with open(motd_path, "r") as fp:
            lines = len(fp.readlines())
            pick = randrange(1, lines)
            motto = linecache.getline(motd_path, pick)
            motto_out = motto.strip("\n")
            print([pick, ": ", motto_out])
            print(src)
            print(motd_path)
            # Manipulate text source
            source = obs.obs_get_source_by_name(src)
            settings = obs.obs_data_create()
            obs.obs_data_set_string(settings, "text", motto_out)
            obs.obs_source_update(source, settings)
            obs.obs_data_release(settings)
            obs.obs_source_release(source)
        loop()

def loop():
    obs.timer_remove(pickamotto)
    obs.timer_add(pickamotto, refresh * 1000)

def script_unload():
    source = obs.obs_get_source_by_name(src)
    obs.obs_source_release(source)
